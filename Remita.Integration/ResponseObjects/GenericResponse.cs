﻿namespace Remita.Integration.ResponseObjects
{
    public class GenericResponse
    {
        public string status { get; set; }
        public GenericData data { get; set; }
    }
    public class GenericData
    {
        public string responseId { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public object data { get; set; }
    }
}
