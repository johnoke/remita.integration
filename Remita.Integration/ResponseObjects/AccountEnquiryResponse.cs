﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remita.Integration.ResponseObjects
{
    public class AccountEnquiryResponse
    {
        public string status { get; set; }
        public AccountEnquiryData data { get; set; }
    }
    public class AccountEnquiryData : GenericData
    {
        public string accountNo { get; set; }
        public string accountName { get; set; }
        public string bankCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
    }
}
