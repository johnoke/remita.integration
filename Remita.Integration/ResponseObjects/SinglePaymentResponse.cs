﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remita.Integration.ResponseObjects
{
    public class SinglePaymentResponse
    {
        public string status { get; set; }
        public SinglePaymentResponseData data { get; set; }
    }
    public class SinglePaymentResponseData : GenericData
    {
        public string authorizationId { get; set; }
        public string transRef { get; set; }
        public string transDate { get; set; }
        public string paymentDate { get; set; }

    }
    public class SinglePaymentStatusResponse
    {
        public string status { get; set; }
        public SinglePaymentStatusResponseData data { get; set; }
    }
    public class SinglePaymentStatusResponseData
    {
        public string authorizationId { get; set; }
        public string transRef { get; set; }
        public string debitAccount { get; set; }
        public string toBank { get; set; }
        public string creditAccount { get; set; }
        public string narration { get; set; }
        public string amount { get; set; }
        public string paymentStatus { get; set; }
        public string settlementDate { get; set; }
        public string paymentDate { get; set; }
        public string currencyCode { get; set; }
        public string paymentStatusCode { get; set; }
    }
}
