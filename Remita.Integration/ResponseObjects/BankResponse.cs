﻿using System.Collections.Generic;

namespace Remita.Integration.ResponseObjects
{
    public class BankResponse
    {
        public string status { get; set; }
        public BankData data { get; set; }
    }
    public class BankData
    {
        public string responseId { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public List<BankObject> banks { get; set; }
    }
    public class BankObject
    {
        public string bankCode { get; set; }
        public string bankName { get; set; }
        public string bankAccronym { get; set; }
        public string type { get; set; }
    }
}
