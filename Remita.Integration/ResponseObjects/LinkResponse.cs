﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remita.Integration.ResponseObjects
{
    public class LinkResponse
    {
        public string status { get; set; }
        public LinkResponseData data { get; set; }
    }
    public class LinkResponseData : GenericData
    {
        public string remitaTransRef { get; set; }
        public List<Dictionary<string, string>> authParams { get; set; }
        public string mandateNumber { get; set; }
    }
    public class ValidateLinkResponse
    {
        public string status { get; set; }
        public ValidateLinkResponseData data { get; set; }
    }
    public class ValidateLinkResponseData : GenericData
    {
        public string remitaTransRef { get; set; }
        public string accountToken { get; set; }
    }
}
