﻿using Remita.Integration.Utilities;
namespace Remita.Integration.DTOs
{
    public class UnlinkAccount
    {
        public string accountToken { get; set; }
        public UnlinkAccount(string accountToken, Driver driver)
        {
            this.accountToken = EncryptionHelper.EncryptText(accountToken, driver.EncryptionKey, driver.EncryptionVector);
        }
    }
}
