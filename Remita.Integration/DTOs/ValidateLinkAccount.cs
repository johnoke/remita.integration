﻿using Remita.Integration.Utilities;
using System.Collections.Generic;

namespace Remita.Integration.DTOs
{
    public class ValidateLinkAccount
    {
        public string remitaTransRef { get; set; }
        public List<Dictionary<string, string>> authParams { get; set; }
        public ValidateLinkAccount(string remitaTransRef, List<Dictionary<string, string>> authParams, Driver driver)
        {
            this.remitaTransRef = EncryptionHelper.EncryptText(remitaTransRef, driver.EncryptionKey, driver.EncryptionVector);
            this.authParams = new List<Dictionary<string, string>>();
            foreach (var param in authParams)
            {
                var dict = new Dictionary<string, string>();
                foreach (var data in param)
                {
                    var encryptedValue = EncryptionHelper.EncryptText(data.Value, driver.EncryptionKey, driver.EncryptionVector);
                    dict.Add(data.Key, encryptedValue);
                }
                this.authParams.Add(dict);
            }
        }

    }
}
