﻿using Remita.Integration.Utilities;

namespace Remita.Integration.DTOs
{
    public class SinglePaymentRequestStatus
    {
        public string transRef { get; set; }
        public SinglePaymentRequestStatus(string transRef, Driver driver)
        {
            this.transRef = EncryptionHelper.EncryptText(transRef, driver.EncryptionKey, driver.EncryptionVector);
        }
    }
}
