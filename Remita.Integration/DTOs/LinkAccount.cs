﻿using Remita.Integration.Utilities;

namespace Remita.Integration.DTOs
{
    public class LinkAccount
    {
        public LinkAccount(string acctNo, string bankCode, Driver driver)
        {
            this.accountNo = EncryptionHelper.EncryptText(acctNo, driver.EncryptionKey, driver.EncryptionVector);
            this.bankCode = EncryptionHelper.EncryptText(bankCode, driver.EncryptionKey, driver.EncryptionVector);
        }
        public string accountNo { get; set; }
        public string bankCode { get; set; }
    }
}
