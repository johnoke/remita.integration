﻿using Remita.Integration.Utilities;

namespace Remita.Integration.DTOs
{
    public class SinglePaymentRequest
    {
        public string debitAccountToken { get; set; }
        public string toBank { get; set; }
        public string creditAccount { get; set; }
        public string narration { get; set; }
        public string amount { get; set; }
        public string transRef { get; set; }
        public SinglePaymentRequest(string debitAccountToken, string toBank, string creditAccount, string narration, string amount, string transRef, Driver driver)
        {
            this.debitAccountToken = EncryptionHelper.EncryptText(debitAccountToken, driver.EncryptionKey, driver.EncryptionVector);
            this.toBank = EncryptionHelper.EncryptText(toBank, driver.EncryptionKey, driver.EncryptionVector);
            this.creditAccount = EncryptionHelper.EncryptText(creditAccount, driver.EncryptionKey, driver.EncryptionVector);
            this.narration = EncryptionHelper.EncryptText(narration, driver.EncryptionKey, driver.EncryptionVector);
            this.amount = EncryptionHelper.EncryptText(amount, driver.EncryptionKey, driver.EncryptionVector);
            this.transRef = EncryptionHelper.EncryptText(transRef, driver.EncryptionKey, driver.EncryptionVector);
        }
    }
}
