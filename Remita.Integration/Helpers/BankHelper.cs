﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Remita.Integration.Utilities;
using Remita.Integration.Constants;
using System;
using Remita.Integration.Operations;
using Remita.Integration.DTOs;

namespace Remita.Integration.Helpers
{
    public class BankHelper
    {
        public Dictionary<string, BankOperation> resources = new Dictionary<string, BankOperation>();
        public BankHelper()
        {
            resources.Add("staging", new BankOperation
            {
                GetBanks = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/fi/banks",
                Enquiry = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/fi/account/lookup"
            });
            resources.Add("production", new BankOperation
            {
                GetBanks = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/fi/banks",
                Enquiry = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/fi/account/lookup"
            });
        }
        public async Task<string> GetBanks(Driver driver)
        {
            string resource = resources[driver.Environment].GetBanks;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, headers: headers);
            return response;
        }
        public async Task<string> Enquiry(string bankCode, string accountNumber, Driver driver)
        {
            string resource = resources[driver.Environment].Enquiry;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            LinkAccount linkAccount = new LinkAccount(accountNumber, bankCode, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: linkAccount, headers: headers);
            return response;
        }
    }
}
