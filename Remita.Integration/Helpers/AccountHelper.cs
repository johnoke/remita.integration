﻿using Remita.Integration.Constants;
using Remita.Integration.DTOs;
using Remita.Integration.Operations;
using Remita.Integration.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Remita.Integration.Helpers
{
    public class AccountHelper
    {
        public Dictionary<string, AccountOperation> resources = new Dictionary<string, AccountOperation>();
        public AccountHelper()
        {
            resources.Add("staging", new AccountOperation {
                Link = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/init",
                ValidateLink = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/validate",
                Unlink = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/del",
                SinglePayment = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/payment/charge",
                SinglePaymentStatus = "http://www.remitademo.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/payment/status"
            });
            resources.Add("production", new AccountOperation
            {
                Link = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/init",
                ValidateLink = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/validate",
                Unlink = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/account/token/del",
                SinglePayment = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/payment/charge",
                SinglePaymentStatus = "https://login.remita.net/remita/exapp/api/v1/send/api/rpgsvc/rpg/api/v2/merc/payment/status"
            });
        }
        public async Task<string> LinkAccount(string bankCode, string accountNumber, Driver driver)
        {
            string resource = resources[driver.Environment].Link;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            LinkAccount linkAccount = new LinkAccount(accountNumber, bankCode, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: linkAccount, headers: headers);
            return response;
        }
        public async Task<string> ValidateLinkAccount(string transRef, List<Dictionary<string, string>> authParams, Driver driver)
        {
            string resource = resources[driver.Environment].ValidateLink;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            ValidateLinkAccount data = new ValidateLinkAccount(transRef, authParams, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: data, headers: headers);
            return response;
        }
        public async Task<string> UnlinkAccount(string token, Driver driver)
        {
            string resource = resources[driver.Environment].Unlink;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            UnlinkAccount data = new UnlinkAccount(token, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: data, headers: headers);
            return response;
        }
        public async Task<string> SinglePayment(string debitAccountToken, string toBank, string creditAccount, string narration, string amount, string transRef, Driver driver)
        {
            string resource = resources[driver.Environment].SinglePayment;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            SinglePaymentRequest data = new SinglePaymentRequest(debitAccountToken, toBank, creditAccount, narration, amount, transRef, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: data, headers: headers);
            return response;
        }
        public async Task<string> SinglePaymentStatus(string transRef, Driver driver)
        {
            string resource = resources[driver.Environment].SinglePaymentStatus;
            var headers = GeneralUtil.AddHeaders(driver, DateTime.Now.ToString("ddMMyyyyhhmmsstt"));
            SinglePaymentRequestStatus data = new SinglePaymentRequestStatus(transRef, driver);
            var response = await new ApiRequest(resource).MakeRequest(method: Verbs.POST, data: data, headers: headers);
            return response;
        }
    }
}

    