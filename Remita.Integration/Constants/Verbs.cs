﻿using System;
namespace Remita.Integration.Constants
{
    public class Verbs
    {
        public const string GET = "GET";
        public const string POST = "POST";
    }
}
