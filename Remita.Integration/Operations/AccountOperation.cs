﻿namespace Remita.Integration.Operations
{
    public class AccountOperation
    {
        public string Link { get; set; }
        public string ValidateLink { get; set; }
        public string Unlink { get; set; }
        public string Charge { get; set; }
        public string SinglePayment { get; set; }
        public string SinglePaymentStatus { get; set; }
    }
}
