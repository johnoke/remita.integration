﻿namespace Remita.Integration.Operations
{
    public class BankOperation
    {
        public string GetBanks { get; set; }
        public string Enquiry { get; set; }
    }
}
