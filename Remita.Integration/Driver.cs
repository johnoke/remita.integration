﻿namespace Remita.Integration
{
    public class Driver
    {
        public string ApiKey { get; set; }
        public string UserToken { get; set; }
        public string EncryptionKey { get; set; }
        public string EncryptionVector { get; set; }
        public string Environment { get; set; }
        public string MerchantId { get; set; }
        public Driver(string apiKey, string userToken, string encryptionKey, string encryptionVector, string merchantId, string environment = "staging")
        {
            ApiKey = apiKey;
            UserToken = userToken;
            EncryptionKey = encryptionKey;
            EncryptionVector = encryptionVector;
            Environment = environment;
            MerchantId = merchantId;
        }
    }
}
