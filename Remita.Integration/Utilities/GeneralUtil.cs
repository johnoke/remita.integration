﻿using System;
using System.Collections.Generic;

namespace Remita.Integration.Utilities
{
    public class GeneralUtil
    {
        public static List<KeyValuePair<string, string>> AddHeaders(Driver driver, string requestId)
        {
            var headers = new List<KeyValuePair<string, string>>();
            int hours = TimeZoneInfo.Local.BaseUtcOffset.Hours;
            string offset = string.Format("{0}{1}", ((hours > 0) ? "+" : ""), hours.ToString("00"));
            string isoformat = DateTime.Now.ToString("s") + offset + "00";
            var apiDetailsHash = EncryptionHelper.SHA512Hash($"{driver.ApiKey}{requestId}{driver.UserToken}");
            headers.Add(new KeyValuePair<string, string>("API_KEY", driver.ApiKey));
            headers.Add(new KeyValuePair<string, string>("MERCHANT_ID", driver.MerchantId));
            headers.Add(new KeyValuePair<string, string>("REQUEST_ID", requestId));
            headers.Add(new KeyValuePair<string, string>("REQUEST_TS", isoformat));
            headers.Add(new KeyValuePair<string, string>("API_DETAILS_HASH", apiDetailsHash.ToLower()));
            return headers;
        }
    }
}
